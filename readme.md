# Selnium Instagram

Proyek `Selenium Instagram` adalah sebuah alat untuk scraping data dari Instagram menggunakan Selenium. Alat ini memungkinkan pengguna untuk melakukan login, scrolling, dan ekstraksi data postingan serta komentar dari Instagram.

## Struktur Proyek
```
SeleniumInstagram/
├── cookies/
│ └── file_cookies.json
├── driver/
│ ├── filedriver.exe
│ └── filedriver
├── lib/
│ ├── init.py
│ ├── comment_data_extract.py
│ ├── login.py
│ ├── post_data_extract.py
│ ├── proses.py
│ ├── run_scraper.py
│ ├── utils.py
│ └── xpath.py
├── .env
├── keywords.txt
├── main.py
├── requirements.txt
```

### Deskripsi Direktori dan Berkas

- **cookies/**: Direktori untuk menyimpan cookies yang diperlukan untuk login.
  - `file_cookies.json`: File cookies yang digunakan untuk menyimpan sesi login Instagram.
  
- **driver/**: Direktori untuk menyimpan driver Selenium.
  - `filedriver.exe`: Driver Chrome untuk Windows.
  - `filedriver`: Driver Chrome untuk Linux/Mac.
  
- **lib/**: Direktori yang berisi modul dan skrip utama.
  - `__init__.py`: Berkas inisialisasi untuk paket Python.
  - `comment_data_extract.py`: Modul untuk mengekstrak data komentar.
  - `login.py`: Modul untuk mengelola proses login.
  - `post_data_extract.py`: Modul untuk mengekstrak data postingan.
  - `proses.py`: Modul untuk memproses data postingan dan komentar.
  - `run_scraper.py`: Modul untuk menjalankan scraper.
  - `utils.py`: Modul yang berisi fungsi utilitas.
  - `xpath.py`: Modul yang berisi definisi XPath.

- **.env**: Berkas konfigurasi untuk menyimpan variabel lingkungan.
- **keywords.txt**: Berkas teks yang berisi daftar kata kunci untuk pencarian Instagram.
- **main.py**: Skrip utama untuk menjalankan aplikasi.
- **requirements.txt**: Daftar dependensi Python yang diperlukan.

## Persyaratan

Pastikan Anda telah menginstal dependensi yang diperlukan dengan menjalankan perintah berikut:
```
pip install -r requirements.txt
```

### Penggunaan

1. Menyiapkan Lingkungan:

 - Pastikan Anda memiliki Python dan pip terinstal di sistem Anda.
 - Instal semua dependensi yang diperlukan dengan menjalankan pip install -r requirements.txt.

2. Menjalankan Aplikasi:

 - Jalankan skrip main.py untuk memulai scraping data dari Instagram.
 ```
 python main.py
 ```

### Catatan
 - `Driver Chrome`: Pastikan Anda memiliki driver Chrome yang sesuai untuk sistem operasi Anda di direktori driver/.
 - `Variabel Lingkungan`: Gunakan file .env untuk menyimpan variabel lingkungan yang diperlukan
