from fake_headers import Headers
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
import os
from dotenv import load_dotenv

load_dotenv()

def get_user_agent() -> str:
    """
    Mendapatkan atau mengambil string User-Agent acak.

    Menghasilkan:
        str: String User-Agent acak.
    """
    headers = Headers(headers=True)
    return headers.generate()['User-Agent']

def scroll_and_get_links(driver) -> list:
    """
    Melakukan scroll halaman ke bawah dan mendapatkan semua tautan unik di halaman tersebut.

    Argumen:
        driver: Instans Selenium WebDriver.

    Menghasilkan:
        list: Daftar tautan unik yang ditemukan di halaman.
    """
    try:
        all_links = []
        visited_links = set()
        last_height = driver.execute_script("return document.documentElement.scrollHeight")

        while True:
            driver.execute_script("window.scrollTo(0, document.documentElement.scrollHeight);")
            time.sleep(5)

            try:
                link_elements = WebDriverWait(driver, 10).until(
                    EC.presence_of_all_elements_located((By.XPATH, "//div[contains(@class, '_ac7v xzboxd6 xras4av xgc1b0m')]//a[contains(@class, 'x1i10hfl')]"))
                )

                new_links = [link.get_attribute("href") for link in link_elements]
                unique_links = set(new_links) - visited_links
                all_links.extend(unique_links)
                visited_links.update(unique_links)

                new_height = driver.execute_script("return document.documentElement.scrollHeight")
                if new_height == last_height:
                    break
                last_height = new_height

            except Exception as e:
                print(f"Terjadi kesalahan saat mengambil tautan: {str(e)}")
                continue

        return all_links

    except Exception as e:
        print(f"Terjadi kesalahan: {str(e)}")
        return []

def read_text_file() -> str:
    """
    Membaca isi dari file teks tertentu.

    Menghasilkan:
        str: Isi dari file, atau string kosong jika file tidak ditemukan atau terjadi kesalahan.
    """
    file_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../keywords.txt"))
    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            content = file.read()
        return content
    except FileNotFoundError:
        print(f"File di {file_path} tidak ditemukan.")
        return ""
    except IOError as e:
        print(f"Terjadi kesalahan saat membaca file: {str(e)}")
        return ""
