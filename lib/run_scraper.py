from lib import InstagramLogin as Login
from lib.proses import ProsesPostAndComment
from lib.utils import read_text_file, get_user_agent, scroll_and_get_links

from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.core.os_manager import ChromeType

import sys
import os
import platform
import time

class Instagram:
    def __init__(self ):
        self.instagram_login = Login()
        self.keywords = read_text_file()
        self.user_agents = get_user_agent() if get_user_agent() else "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36"
        self.scroll_and_links = []
             
    def configure_browser_options(self) -> webdriver.ChromeOptions:
        """
        Konfigurasi opsi browser untuk Selenium.

        Mengembalikan:
            webdriver.ChromeOptions: Opsi browser yang sudah dikonfigurasi.
        """
        browser_options = webdriver.ChromeOptions()
        browser_options.add_argument('--headless')
        browser_options.add_argument("--no-sandbox")
        browser_options.add_argument("--lang=en-US")
        browser_options.add_argument("--disable-dev-shm-usage")
        browser_options.add_argument("--disable-gpu")
        browser_options.add_argument("--log-level=3")
        browser_options.add_argument("--silent")
        
        # Set User-Agent
        browser_options.add_argument(f"--user-agent={self.user_agents}")
        
        return browser_options

    def initialize_driver(self) -> webdriver.Chrome:
        """
        Inisialisasi driver Selenium.

        Returns:
            webdriver.Chrome: Instance Selenium WebDriver yang sudah diinisialisasi.

        Raises:
            FileNotFoundError: Jika file driver Chrome tidak ditemukan.
            WebDriverException: Jika terjadi kesalahan saat menginisialisasi driver, driver akan melakukan install dari webdriver_manager.
        """
        try:
            chrome_driver_path = os.path.abspath(
                os.path.join(os.path.dirname(__file__), "..", "driver", "chromedriver.exe")
            )
            if platform.system() in ["Linux", "Darwin"]:
                chrome_driver_path = os.path.abspath(
                    os.path.join(os.path.dirname(__file__), "..", "driver", "chromedriver")
                )

            options = self.configure_browser_options()
            service = ChromeService(executable_path=chrome_driver_path)
            driver = webdriver.Chrome(options=options, service=service)
            return driver

        except FileNotFoundError as e:
            print(f"File driver Chrome tidak ditemukan/tidak cocok dengan versi browser: {e}")
            sys.exit(1)
        
        except WebDriverException as e:
            try:
                print("Mencoba untuk menginstal... Driver Chrome dari webdriver_manager.")
                chrome_driver_path = ChromeDriverManager().install()
                print("Driver Chrome berhasil diinstal.")
                chrome_service = ChromeService(executable_path=chrome_driver_path)
                options = self.configure_browser_options()
                driver = webdriver.Chrome(options=options, service=chrome_service)
                return driver
            except Exception as e:
                print(f"Ada kesalahan saat melakukan install driver dari webdriver_manager: {e}")
                sys.exit(1)
                
    def measure_time(method):
        """
        Decorator untuk mengukur waktu eksekusi sebuah metode.
        """
        def wrapper(self, *args, **kwargs):
            start_time = time.time()
            result = method(self, *args, **kwargs)
            end_time = time.time()
            elapsed_time = end_time - start_time
            print(f"Waktu yang diperlukan untuk {method.__name__}: {elapsed_time:.2f} detik")
            return result
        return wrapper
    
    @measure_time        
    def run_instagram_scraper(self) -> bool:
        """
        Jalankan scraper Instagram.

        Metode ini menginisialisasi driver Selenium, melakukan login menggunakan cookies atau login manual,
        dan kemudian melakukan scrolling melalui setiap halaman dengan kata kunci untuk mengambil link postingan.

        Kembalian:
            bool: True jika scraper berhasil, False jika gagal.
        """
        try:
            # Initialize the Selenium driver
            driver = self.initialize_driver()
            if not driver:
                print("Failed to initialize Selenium driver.")
                return False

            # Login with cookies or manual login
            if not self.instagram_login.login_with_cookies(driver) and not self.instagram_login.login_with_manual(driver):
                print("Instagram login failed")
                return False

            driver.refresh()
            time.sleep(2)
            print("Instagram login successful")

            if self.keywords:
                for keyword in self.keywords.splitlines():
                    try:
                        print(f"Processing keyword: {keyword}")
                        driver.get(f"https://www.instagram.com/{keyword}/")

                        # Scroll and get links
                        post_links = scroll_and_get_links(driver)
                        num_links = len(post_links)
                        print(f"Number of links for {keyword}: {num_links}")

                        if num_links > 0:
                            scraper = ProsesPostAndComment(driver, post_links, keyword)
                            scraper.scrape_post_comments()
                    except Exception as e:
                        print(f"Error processing keyword {keyword}: {e}")

            return True

        except Exception as e:
            print(f"An error occurred: {e}")
            return False

        finally:
            if driver:
                driver.quit()