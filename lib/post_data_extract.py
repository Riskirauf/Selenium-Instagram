# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.common.exceptions import TimeoutException
# from lib.xpath import XpathPost, XpathComment
# from datetime import datetime
# import re

# class PostDataExtract:
#     """
#     Class untuk mengekstrak informasi dari postingan (post) pada media sosial dari postingan Instagram.
    
#     Args:
#         driver: Instance WebDriver yang digunakan untuk mengontrol browser.
    
#     Attributes:
#         wait : Instance WebDriverWait untuk menunggu elemen muncul pada halaman.
        
#     Methods:
#         handle_timeout_exceptions(default_value="-"): Menangani TimeoutException dan mengembalikan nilai default.
#         post_username(): Ekstrak nama pengguna dari posting.
#         post_content(): Ekstrak teks postingan.
#         post_date(): Ekstrak tanggal dari postingan.
#         post_likes(): Ekstrak jumlah like dari postingan.
#         post_image(): Ekstrak URL gambar dari postingan.
#         post_comment_count(): Ekstrak jumlah komentar dari postingan.
#     """
    
#     def __init__(self, driver):
#         self.wait = WebDriverWait(driver, 10)
        
#     def handle_timeout_exceptions(self, default_value="-"):
#         """
#         Menangani TimeoutException dan mengembalikan nilai default.
        
#         Args:
#             default_value (str, optional): Nilai default yang akan dikembalikan. Defaults to "-".
        
#         Returns:
#             str: Nilai default.
#         """
#         try:
#             raise TimeoutException
#         except TimeoutException:
#             return default_value
        
#     def post_username(self) -> str:
#         """
#         Ekstrak nama pengguna dari postingan.
        
#         Returns:
#             str: Nama pengguna yang membuat postingan, atau nilai default jika tidak ditemukan.
#         """
#         try:
#             username_elements = self.wait.until(
#                 EC.presence_of_all_elements_located((By.XPATH, XpathPost.post_user_name))
#             )
            
#             if not username_elements:
#                 username_elements = self.wait.until(
#                     EC.presence_of_all_elements_located((By.XPATH, XpathPost.post_user_name_alternatif))
#                 )
                
#             username = username_elements[0].text if username_elements else "-"
#         except TimeoutException:
#             username = self.handle_timeout_exceptions()

#         return username
    
#     def post_content(self) -> str:
#         """
#         Ekstrak teks dari postingan.
        
#         Returns:
#             str: Teks dari postingan, atau nilai default jika tidak ditemukan.
#         """
#         try:
#             content_elements = self.wait.until(
#                 EC.presence_of_all_elements_located((By.XPATH, XpathPost.post_content))
#             )
            
#             if not content_elements:
#                 content_elements = self.wait.until(
#                     EC.presence_of_all_elements_located((By.XPATH, XpathPost.post_content_alternatif))
#                 )
                
#             content = content_elements[0].text
#             content = content.replace("\n", " ").replace("\r", "")
#             content = content.strip()
#         except TimeoutException:
#             content = self.handle_timeout_exceptions()
        
#         return content
    
#     def post_date(self) -> str:
#         """
#         Ekstrak tanggal dari postingan.
        
#         Returns:
#             str: Tanggal postingan dalam format "%Y-%m-%d %H:%M:%S", atau nilai default jika tidak ditemukan.
#         """
#         try:
#             date_elements = self.wait.until(
#                 EC.presence_of_all_elements_located((By.XPATH, XpathPost.post_date))
#             )
            
#             date_post = date_elements[0].get_attribute("datetime")
#             date_post = date_post.replace("T", " ").replace(".000Z", "")
#         except TimeoutException:
#             date_post = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
#             date_post = self.handle_timeout_exceptions(default_value=date_post)
            
#         return date_post
    
#     def post_likes(self) -> int:
#         """
#         Ekstrak jumlah like dari postingan.
        
#         Returns:
#             int: Jumlah like dari postingan, atau nilai default jika tidak ditemukan.
#         """
#         try:
#             likes_elements = self.wait.until(
#                 EC.presence_of_all_elements_located((By.XPATH, XpathPost.post_like))
#             )
        
#             like_text = likes_elements[0].text
#             matches = re.findall(r'\d+', like_text)

#             like = int(matches[0]) if matches else 0
#         except TimeoutException:
#             like = int(self.handle_timeout_exceptions(default_value="0"))

#         return like
    
#     def post_image(self) -> str:
#         """
#         Ekstrak URL gambar dari postingan.
        
#         Returns:
#             str: URL gambar dari postingan, atau nilai default jika tidak ditemukan.
#         """
#         try:
#             image_elements = self.wait.until(
#                 EC.presence_of_all_elements_located((By.XPATH, XpathPost.post_img))
#             )
#             if image_elements:
#                 image = image_elements[0].get_attribute("src")
#             else:
#                 image = self.handle_timeout_exceptions()
#         except TimeoutException:
#             image = self.handle_timeout_exceptions()
        
#         return image
    
#     def post_comment_count(self) -> int:
#         """
#         Ekstrak jumlah komentar dari postingan.
        
#         Returns:
#             int: Jumlah komentar dari postingan, atau nilai default jika tidak ditemukan.
#         """
#         try:
#             content_comment_elements = self.wait.until(
#                 EC.presence_of_all_elements_located((By.XPATH, XpathComment.comment_content))
#             )
            
#             content_comment_count = len(content_comment_elements)
#         except TimeoutException:
#             content_comment_count = int(self.handle_timeout_exceptions(default_value="0"))
        
#         return content_comment_count

################################################

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from lib.xpath import XpathPost, XpathComment
from datetime import datetime
import re

class PostDataExtract:
    """
    Class untuk mengekstrak informasi dari postingan (post) pada media sosial dari postingan Instagram.
    
    Args:
        driver: Instance WebDriver yang digunakan untuk mengontrol browser.
    
    Attributes:
        wait : Instance WebDriverWait untuk menunggu elemen muncul pada halaman.
        
    Methods:
        handle_timeout_exceptions(default_value="-"): Menangani TimeoutException dan mengembalikan nilai default.
        find_element_with_fallback(by, value, alt_value): Mencari elemen dengan fallback ke nilai alternatif jika elemen tidak ditemukan.
        find_elements_with_fallback(by, value): Mencari elemen-elemen dengan fallback jika elemen tidak ditemukan.
        post_username(): Ekstrak nama pengguna dari posting.
        post_content(): Ekstrak teks postingan.
        post_date(): Ekstrak tanggal dari postingan.
        post_likes(): Ekstrak jumlah like dari postingan.
        post_image(): Ekstrak URL gambar dari postingan.
        post_comment_count(): Ekstrak jumlah komentar dari postingan.
    """
    
    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(driver, 5)
        
    def handle_timeout_exceptions(self, default_value="-"):
        """
        Menangani TimeoutException dan mengembalikan nilai default.
        
        Args:
            default_value (str, optional): Nilai default yang akan dikembalikan. Defaults to "-".
        
        Returns:
            str: Nilai default.
        """
        return default_value

    def find_element_with_fallback(self, by, value, alt_value=None):
        """
        Mencari elemen dengan fallback ke nilai alternatif jika elemen tidak ditemukan.
        
        Args:
            by (By): Tipe pencarian (misalnya, By.XPATH).
            value (str): Nilai dari tipe pencarian (misalnya, xpath dari elemen).
            alt_value (str, optional): Nilai alternatif dari tipe pencarian.
        
        Returns:
            WebElement: Elemen yang ditemukan.
        """
        try:
            element = self.wait.until(EC.presence_of_element_located((by, value)))
        except TimeoutException:
            if alt_value:
                try:
                    element = self.wait.until(EC.presence_of_element_located((by, alt_value)))
                except TimeoutException:
                    element = None
            else:
                element = None
        return element

    def find_elements_with_fallback(self, by, value):
        """
        Mencari elemen-elemen dengan fallback jika elemen tidak ditemukan.
        
        Args:
            by (By): Tipe pencarian (misalnya, By.XPATH).
            value (str): Nilai dari tipe pencarian (misalnya, xpath dari elemen).
        
        Returns:
            list: Daftar elemen yang ditemukan.
        """
        try:
            elements = self.wait.until(EC.presence_of_all_elements_located((by, value)))
        except (TimeoutException, NoSuchElementException):
            elements = []
        return elements
    
    def post_username(self) -> str:
        """
        Ekstrak nama pengguna dari postingan.
        
        Returns:
            str: Nama pengguna yang membuat postingan, atau nilai default jika tidak ditemukan.
        """
        username_element = self.find_element_with_fallback(By.XPATH, XpathPost.post_user_name, XpathPost.post_user_name_alternatif)
        return username_element.text if username_element else self.handle_timeout_exceptions()
    
    def post_content(self) -> str:
        """
        Ekstrak teks dari postingan.
        
        Returns:
            str: Teks dari postingan, atau nilai default jika tidak ditemukan.
        """
        content_element = self.find_element_with_fallback(By.XPATH, XpathPost.post_content, XpathPost.post_content_alternatif)
        if content_element:
            content = content_element.text.replace("\n", " ").replace("\r", "").strip()
        else:
            content = self.handle_timeout_exceptions()
        return content
    
    def post_date(self) -> str:
        """
        Ekstrak tanggal dari postingan.
        
        Returns:
            str: Tanggal postingan dalam format "%Y-%m-%d %H:%M:%S", atau nilai default jika tidak ditemukan.
        """
        date_element = self.find_element_with_fallback(By.XPATH, XpathPost.post_date)
        if date_element:
            date_post = date_element.get_attribute("datetime").replace("T", " ").replace(".000Z", "")
        else:
            date_post = self.handle_timeout_exceptions(default_value=datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        return date_post
    
    def post_likes(self) -> int:
        """
        Ekstrak jumlah like dari postingan.
        
        Returns:
            int: Jumlah like dari postingan, atau nilai default jika tidak ditemukan.
        """
        likes_element = self.find_element_with_fallback(By.XPATH, XpathPost.post_like)
        if likes_element:
            like_text = likes_element.text
            matches = re.findall(r'\d+', like_text)
            like = int(matches[0]) if matches else 0
        else:
            like = int(self.handle_timeout_exceptions(default_value="0"))
        return like
    
    def post_image(self) -> str:
        """
        Ekstrak URL gambar dari postingan.
        
        Returns:
            str: URL gambar dari postingan, atau nilai default jika tidak ditemukan.
        """
        image_element = self.find_element_with_fallback(By.XPATH, XpathPost.post_img)
        return image_element.get_attribute("src") if image_element else self.handle_timeout_exceptions()
    
    def post_comment_count(self) -> int:
        """
        Ekstrak jumlah komentar dari postingan.
        
        Returns:
            int: Jumlah komentar dari postingan, atau nilai default jika tidak ditemukan.
        """
        comment_elements = self.find_elements_with_fallback(By.XPATH, XpathComment.comment_content)
        return len(comment_elements) if comment_elements else int(self.handle_timeout_exceptions(default_value="0"))

