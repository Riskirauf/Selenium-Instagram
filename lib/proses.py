import time
import os
from datetime import datetime
import json

from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from lib.post_data_extract import PostDataExtract
from lib.comment_data_extract import CommentDataExtract

class ProsesPostAndComment:
    """
    Kelas untuk memproses postingan dan komentar Instagram.

    Argumen:
        driver: Instance dari Selenium WebDriver.
        post_links: Daftar tautan postingan Instagram yang akan diproses.
        key: Kata kunci untuk mengidentifikasi sesi pemrosesan.
    """

    def __init__(self, driver, post_links, key):
        self.driver = driver
        self.links = post_links
        self.key = key
        self.post = PostDataExtract(driver)
        self.comment = CommentDataExtract(driver)
        self.elements_replies_xpath = ("//div[contains(@class, 'x9f619 xjbqb8w x78zum5 x168nmei x13lgxp2 x5pf9jr "
                                       "xo71vjh x1uhb9sk x1plvlek xryxfnj x1c4vz4f x2lah0s xdt5ytf xqjyukv')]//div"
                                       "[contains(@class, 'x9f619 xjbqb8w x78zum5 x168nmei x13lgxp2 x5pf9jr xo71vjh "
                                       "x1uhb9sk x1plvlek xryxfnj x1c4vz4f x2lah0s xdt5ytf xqjyukv x1qjc9v5 x1oa3qoh "
                                       "x1nhvcw1 x540dpk')]/div")

    def view_replies(self):
        """
        Klik semua tombol balasan yang ditemukan di halaman untuk memunculkan semua balasan.
        """
        try:
            elements_replies = WebDriverWait(self.driver, 10).until(
                EC.presence_of_all_elements_located((By.XPATH, self.elements_replies_xpath))
            )
            
            for element in elements_replies:
                self.driver.execute_script("arguments[0].click();", element)
                time.sleep(3)
        except TimeoutException:
            pass

    def save_to_json(self, data):
        """
        Simpan data yang diproses ke dalam file JSON.

        Argumen:
            data: Data yang akan disimpan dalam format JSON.
        """
        try:
            json_folder_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../data_json"))
            if not os.path.exists(json_folder_path):
                os.makedirs(json_folder_path)

            # Buat nama file berdasarkan tanggal saat ini
            current_date = datetime.now().strftime("%Y-%m-%d")
            json_file_name = f"{self.key}_{current_date}.json"
            json_file_path = os.path.join(json_folder_path, json_file_name)

            # Jika file sudah ada, baca isinya terlebih dahulu
            if os.path.exists(json_file_path):
                with open(json_file_path, "r", encoding="utf-8") as json_file:
                    existing_data = json.load(json_file)
            else:
                existing_data = {"datapost": []}

            # Tambahkan data baru ke dalam data yang sudah ada
            existing_data["datapost"].append(data)

            # Simpan kembali data tersebut ke dalam file JSON
            with open(json_file_path, "w", encoding="utf-8") as json_file:
                json.dump(existing_data, json_file, ensure_ascii=False, indent=2)

        except Exception as e:
            print(f"Error saat menyimpan ke JSON: {e}")

    def scroll_comments(self):
        """
        Scroll ke bawah pada elemen komentar untuk memuat semua komentar yang ada.
        """
        comment_section_element = WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.XPATH, "//div[@class='x5yr21d xw2csxc x1odjw0f x1n2onr6']"))
        )

        previous_height = self.driver.execute_script("return arguments[0].scrollHeight", comment_section_element)

        while True:
            self.driver.execute_script("arguments[0].scrollTo(0, arguments[0].scrollHeight);", comment_section_element)
            time.sleep(5)
            new_height = self.driver.execute_script("return arguments[0].scrollHeight", comment_section_element)

            if new_height == previous_height:
                break
            previous_height = new_height

    def scroll_top_comments(self):
        """
        Scroll ke atas pada elemen komentar untuk kembali ke bagian atas komentar.
        """
        comment_section_element = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, "//div[@class='x5yr21d xw2csxc x1odjw0f x1n2onr6']"))
        )

        self.driver.execute_script("arguments[0].scrollTo(0, 0);", comment_section_element)
        time.sleep(2)

    def scrape_post_comments(self):
        """
        Jalankan proses scraping untuk postingan dan komentar.
        """
        for link in self.links:
            time.sleep(3)
            self.driver.get(link)

            self.scroll_comments()
            self.scroll_top_comments()
            self.view_replies()

            post = self.post
            username = post.post_username()
            content = post.post_content()
            date = post.post_date()
            like = post.post_likes()
            url_image = post.post_image()
            comment_count = post.post_comment_count()

            # Simpan data postingan
            post_data = {
                "username": username,
                "content": content,
                "date": date,
                "like": like,
                "comment_count": comment_count,
                "link": link,
                "link image": url_image,
                "comments": []  # Inisialisasi list komentar kosong
            }

            # Simpan data untuk setiap postingan tanpa komentar
            self.save_to_json(data=post_data)
            print(f"Data postingan berhasil disimpan untuk {link}")

            # Jika ada komentar, ambil data komentar dan tambahkan ke dalam data postingan
            if comment_count > 0:
                comment = self.comment
                comment_usernames = comment.comments_username()
                comment_contents = comment.comment_content()
                comment_dates = comment.comment_date()
                comment_likes = comment.comment_like()
                comment_links = comment.comment_link()

                comments_list = []
                for i in range(len(comment_usernames)):
                    comment_data = {
                        "username": comment_usernames[i],
                        "content": comment_contents[i],
                        "date": comment_dates[i],
                        "like": comment_likes[i],
                        "link": comment_links[i],
                    }
                    comments_list.append(comment_data)

                # Perbarui data postingan dengan komentar yang sudah diambil
                post_data["comments"] = comments_list

                # Simpan kembali data postingan beserta komentar ke dalam file JSON
                self.save_to_json(data=post_data)
                print(f"Data postingan dan komentar berhasil disimpan untuk {link}")
