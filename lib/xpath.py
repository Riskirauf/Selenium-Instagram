
class XpathPost():
    """
    class XpathPost implements
    """
    
    post_user_name = "//div[contains(@class, 'x9f619')]/div/span[contains(@class, 'xt0psk2')]//a//span[contains(@class, '_ap3a')]"
    post_user_name_alternatif = "//div[contains(@class, 'xyinxu5 x1pi30zi x1g2khh7 x1swvt13')]//span[contains(@class, '_ap3a _aaco _aacw _aacx _aad7 _aade')]"
    
    post_content = "//div[contains(@class, 'x9f619 xjbqb8w x78zum5 x168nmei x13lgxp2 x5pf9jr xo71vjh x1uhb9sk x1plvlek xryxfnj x1c4vz4f x2lah0s xdt5ytf xqjyukv x1qjc9v5 x1oa3qoh x1nhvcw1')]//span[contains(@class, 'x193iq5w xeuugli x1fj9vlw x13faqbe x1vvkbs xt0psk2 x1i0vuye xvs91rp xo1l8bm x5n08af x10wh9bi x1wdrske x8viiok x18hxmgj')]"
    post_content_alternatif = "//div[contains(@class, 'x9f619 xjbqb8w x78zum5 x168nmei x13lgxp2 x5pf9jr xo71vjh x1uhb9sk x1plvlek xryxfnj x1c4vz4f x2lah0s xdt5ytf xqjyukv x1qjc9v5 x1oa3qoh x1nhvcw1')]//span[contains(@class, 'x193iq5w xeuugli x1fj9vlw x13faqbe x1vvkbs xt0psk2 x1i0vuye xvs91rp xo1l8bm x5n08af x10wh9bi x1wdrske x8viiok x18hxmgj')]"
    
    post_date = "//span[contains(@class,'x193iq5w')]/time[contains(@class, 'xsgj6o6')]"
    post_img = "//div[contains(@class, 'x1lliihq xh8yej3')]//div[contains(@class, '_aagv')]/img"
    post_like = "//section[contains(@class, 'x12nagc')]//span[contains(@class, 'html-span')]"
    
class XpathComment():
    """
    class XpathComment implements
    """
    
    comment_user_name = "//div[contains(@class, 'x78zum5 xdt5ytf x1iyjqo2')]//span[contains(@class, 'xt0psk2')]"
    comment_content = "//div[contains(@class, 'x78zum5 xdt5ytf x1iyjqo2')]//div[contains(@class, 'x9f619 xjbqb8w x78zum5 x168nmei x13lgxp2 x5pf9jr xo71vjh x1uhb9sk x1plvlek xryxfnj x1c4vz4f x2lah0s xdt5ytf xqjyukv x1cy8zhl x1oa3qoh x1nhvcw1')]"
    comment_date = "//div[contains(@class, 'x9f619')]//time[contains(@class, 'x1ejq31n')]"
    comment_likes = "//div[contains(@class, 'x1i10hfl')]//span[contains(@class, 'x1lliihq x193iq5w x6ikm8r x10wlt62 xlyipyv xuxw1ft')]"
    comment_links = "//div[contains(@class, 'x78zum5 xdt5ytf x1iyjqo2')]//div[contains(@class, 'x9f619')]//span/a[contains(@class, 'x1i10hfl')]"