import os
import json
import time
from typing import List, Optional
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from dotenv import load_dotenv

load_dotenv()

class InstagramLogin:
    """
    Kelas untuk menangani login Instagram menggunakan Selenium WebDriver.
    
    Atribut:
        cookies_directory (str): Path ke direktori untuk menyimpan cookies.
        cookies_file (str): Path ke file untuk menyimpan cookies.
    """

    def __init__(self):
        """
        Menginisialisasi kelas InstagramLogin.
        
        Membuat direktori untuk menyimpan cookies jika tidak ada.
        """
        self.cookies_directory = os.path.abspath(os.path.join(os.path.dirname(__file__), "../cookies"))
        self.cookies_file = os.path.join(self.cookies_directory, "instagram_cookies.json")

        if not os.path.exists(self.cookies_directory):
            os.makedirs(self.cookies_directory)

    def login_with_cookies(self, driver: Chrome) -> bool:
        """
        Login ke Instagram menggunakan cookies yang disimpan.
        
        Args:
            driver (Chrome): Instans Selenium Webdriver.
        
        Returns:
            bool: True jika login berhasil, False jika tidak.
        """
        try:
            driver.get("https://www.instagram.com/")
            cookies = self.load_cookies()
            if cookies:
                for cookie in cookies:
                    driver.add_cookie(cookie)
                
                driver.refresh()        
                return True
        except FileNotFoundError:
            print("File cookies tidak ditemukan. Melanjutkan dengan login manual.")
            return False
        except Exception as e:
            print(f"Error selama login dengan cookies: {e}")
            return False
        return False

    def login_with_manual(self, driver: Chrome) -> bool:
        """
        Login manual ke Instagram.
        
        Args:
            driver (Chrome): Instans Selenium Webdriver.
            
        Returns:
            bool: True jika login berhasil, False jika tidak.
        """
        try:
            driver.get("https://www.instagram.com/accounts/login")

            username_input = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.NAME, "username")))
            username_input.send_keys(os.getenv("INSTAGRAM_USERNAME"))
            time.sleep(3)

            password_input = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.NAME, "password")))
            password_input.send_keys(os.getenv("INSTAGRAM_PASSWORD"))
            time.sleep(3)

            submit_button = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//button[@type='submit']")))
            submit_button.click()

            time.sleep(3)

            self.handle_not_now(driver)
            
            time.sleep(3)

            self.save_cookies(driver)

            return True

        except TimeoutException:
            print("Login gagal. Harap periksa kredensial Anda dan coba lagi.")
            return False

    def handle_not_now(self, driver: Chrome):
        """
        Menghandle tombol "Not Now" pada halaman login Instagram.
        
        Args:
            driver (Chrome): Instans Selenium Webdriver.
            
        Returns:
            None
        """
        try:
            not_now_1 = WebDriverWait(driver, timeout=10).until(
                EC.element_to_be_clickable((By.XPATH, "//div[@class='_ac8f']//div[contains(@class, 'x1i10hfl') and contains(@class, 'xa49m3k') and contains(@class, 'xqeqjp1') and contains(@class, 'x2hbi6w') and contains(@class, 'xdl72j9') and contains(@class, 'x2lah0s') and contains(@class, 'xe8uvvx') and contains(@class, 'xdj266r') and contains(@class, 'x11i5rnm') and contains(@class, 'xat24cr') and contains(@class, 'x1mh8g0r') and contains(@class, 'x2lwn1j') and contains(@class, 'xeuugli') and contains(@class, 'x1hl2dhg') and contains(@class, 'xggy1nq') and contains(@class, 'x1ja2u2z') and contains(@class, 'x1t137rt') and contains(@class, 'x1q0g3np') and contains(@class, 'x1lku1pv') and contains(@class, 'x1a2a7pz')]")))
            time.sleep(5)
            not_now_1.click()
            
            not_now_2 = WebDriverWait(driver, timeout=10).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(text(),'Not Now')]"))
            )
            not_now_2.click()
        except TimeoutException:
            print("Tombol 'Not Now' tidak ditemukan. Melanjutkan dengan login manual.")

    def save_cookies(self, driver: Chrome) -> bool:
        """
        Menyimpan cookies ke file.
        
        Args:
            driver (Chrome): Instans Selenium Webdriver.
            
        Returns:
            bool: True jika cookies berhasil disimpan, False jika tidak.
        """
        try:
            cookies = driver.get_cookies()
            with open(self.cookies_file, "w") as file:
                json.dump(cookies, file)
            print("Cookies berhasil disimpan.")
            return True
        except Exception as e:
            print(f"Error saat menyimpan cookies: {e}")
            return False

    def load_cookies(self) -> Optional[List[dict]]:
        """
        Memuat cookies dari file.
        
        Returns:
            Optional[List[dict]]: List of cookies jika file ada, None jika tidak.
        """
        try:
            with open(self.cookies_file, "r") as file:
                cookies = json.load(file)
            print("Cookies berhasil dimuat.")
            return cookies
        except FileNotFoundError:
            print("File cookies tidak ditemukan.")
            return None
        except Exception as e:
            print(f"Error saat memuat cookies: {e}")
            return None