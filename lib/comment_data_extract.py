# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.common.by import By
# from selenium.common.exceptions import TimeoutException, NoSuchElementException
# from datetime import datetime
# from lib.xpath import XpathComment

# class CommentDataExtract:
#     """
#     Class untuk mengekstrak informasi dari komentar pada media sosial Instagram.
    
#     Args:
#         driver: Instance WebDriver yang digunakan untuk mengontrol browser.
    
#     Attributes:
#         wait : Instance WebDriverWait untuk menunggu elemen muncul pada halaman.
        
#     Methods:
#         handle_no_such_element(default_value="-"): Menangani NoSuchElementException dan mengembalikan nilai default.
#         comments_username(): Ekstrak nama pengguna dari komentar.
#         comments_date(): Ekstrak tanggal komentar.
#         comments_text(): Ekstrak teks komentar.
#         comments_likes(): Ekstrak jumlah like pada komentar.
#         comments_links(): Ekstrak jumlah links komentar.   
#     """
    
#     def __init__(self, driver):
#         self.wait = WebDriverWait(driver, 10)
        
#     def handle_no_such_element(self, default_value="-"):
#         """
#         Menangani NoSuchElementException dan mengembalikan nilai default.
        
#         Args:
#             default_value (str, optional): Nilai default yang akan dikembalikan. Defaults to "-".
        
#         Returns:
#             str: Nilai default.
#         """
#         return default_value
        
#     def comments_username(self):
#         """
#         Ekstrak nama pengguna dari komentar.
        
#         Returns:
#             list: Daftar nama pengguna yang berkomentar.
#         """
#         xpath_username = XpathComment.comment_user_name
#         try:
#             username_elements = self.wait.until(
#                 EC.presence_of_all_elements_located((By.XPATH, xpath_username))
#             )
#             comment_username = [username.text for username in username_elements]
#         except (TimeoutException, NoSuchElementException):
#             comment_username = self.handle_no_such_element(default_value=[])
            
#         return comment_username
    
#     def comment_content(self):
#         """
#         Ekstrak teks pengguna dari komentar.
        
#         Returns:
#             list: Daftar teks pengguna yang berkomentar.
#         """
#         xpath_content = XpathComment.comment_content
#         try:
#             content_elements = self.wait.until(
#                 EC.presence_of_all_elements_located((By.XPATH, xpath_content))
#             )
#             comment_content = [content.text for content in content_elements]
#         except (TimeoutException, NoSuchElementException):
#             comment_content = self.handle_no_such_element(default_value=[])
        
#         return comment_content
    
#     def comment_date(self):
#         """
#         Ekstrak tanggal dari komentar.
        
#         Returns:
#             list: Daftar tanggal komentar dalam format "%Y-%m-%d %H:%M:%S".
#         """
#         xpath_date = XpathComment.comment_date
#         try:
#             date_elements = self.wait.until(
#                 EC.presence_of_all_elements_located((By.XPATH, xpath_date))
#             )
#             comment_date = [
#                 datetime.strptime(date.get_attribute("datetime"), "%Y-%m-%dT%H:%M:%S.000Z")
#                 .strftime("%Y-%m-%d %H:%M:%S")
#                 for date in date_elements
#             ]  
#         except (TimeoutException, NoSuchElementException):
#             date_now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
#             comment_date = self.handle_no_such_element(default_value=[date_now])
        
#         return comment_date
    
#     def comment_like(self):
#         """
#         Ekstrak jumlah like dari komentar.
        
#         Returns:
#             list: Daftar jumlah like komentar.
#         """
#         xpath_like = XpathComment.comment_likes
#         try:
#             like_elements = self.wait.until(
#                 EC.presence_of_all_elements_located((By.XPATH, xpath_like))
#             )
#             comment_like = [
#                 like.text.replace("likes", "").replace("like", "").strip() if "like" in like.text else "0"
#                 for like in like_elements
#             ]
#         except (TimeoutException, NoSuchElementException):
#             comment_like = self.handle_no_such_element(default_value=["0"])
        
#         return comment_like
    
#     def comment_link(self):
#         """
#         Ekstrak link dari komentar.
        
#         Returns:
#             list: Daftar link komentar.
#         """
#         xpath_link = XpathComment.comment_links
#         try:
#             link_elements = self.wait.until(
#                 EC.presence_of_all_elements_located((By.XPATH, xpath_link))
#             )
#             comment_link = [link.get_attribute("href") for link in link_elements]
#         except (TimeoutException, NoSuchElementException):
#             comment_link = self.handle_no_such_element(default_value=[])
            
#         return comment_link

#########################

import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from datetime import datetime
from lib.xpath import XpathComment

class CommentDataExtract:
    """
    Class untuk mengekstrak informasi dari komentar pada media sosial Instagram.
    
    Args:
        driver: Instance WebDriver yang digunakan untuk mengontrol browser.
    
    Attributes:
        wait : Instance WebDriverWait untuk menunggu elemen muncul pada halaman.
        
    Methods:
        handle_no_such_element(default_value): Menangani NoSuchElementException dan mengembalikan nilai default.
        find_elements_with_fallback(by, value): Mencari elemen dengan fallback jika elemen tidak ditemukan.
        comments_username(): Ekstrak nama pengguna dari komentar.
        comment_content(): Ekstrak teks pengguna dari komentar.
        comment_date(): Ekstrak tanggal dari komentar.
        comment_like(): Ekstrak jumlah like dari komentar.
        comment_link(): Ekstrak link dari komentar.
    """
    
    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(driver, 10)
        
    def handle_no_such_element(self, default_value):
        """
        Menangani NoSuchElementException dan mengembalikan nilai default.
        
        Args:
            default_value: Nilai default yang akan dikembalikan.
        
        Returns:
            default_value: Nilai default yang diberikan.
        """
        return default_value

    def find_elements_with_fallback(self, by, value):
        """
        Mencari elemen dengan fallback jika elemen tidak ditemukan.
        
        Args:
            by (By): Tipe pencarian (misalnya, By.XPATH).
            value (str): Nilai dari tipe pencarian (misalnya, xpath dari elemen).
        
        Returns:
            list: Daftar elemen yang ditemukan.
        """
        try:
            elements = self.wait.until(EC.presence_of_all_elements_located((by, value)))
        except (TimeoutException, NoSuchElementException):
            elements = []
        return elements

    def comments_username(self):
        """
        Ekstrak nama pengguna dari komentar.
        
        Returns:
            list: Daftar nama pengguna yang berkomentar.
        """
        xpath_username = XpathComment.comment_user_name
        username_elements = self.find_elements_with_fallback(By.XPATH, xpath_username)
        comment_username = [username.text for username in username_elements] if username_elements else self.handle_no_such_element(default_value=[])
        return comment_username
    
    def comment_content(self):
        """
        Ekstrak teks pengguna dari komentar.
        
        Returns:
            list: Daftar teks pengguna yang berkomentar.
        """
        xpath_content = XpathComment.comment_content
        content_elements = self.find_elements_with_fallback(By.XPATH, xpath_content)
        comment_content = [content.text for content in content_elements] if content_elements else self.handle_no_such_element(default_value=[])
        return comment_content
    
    def comment_date(self):
        """
        Ekstrak tanggal dari komentar.
        
        Returns:
            list: Daftar tanggal komentar dalam format "%Y-%m-%d %H:%M:%S".
        """
        xpath_date = XpathComment.comment_date
        date_elements = self.find_elements_with_fallback(By.XPATH, xpath_date)
        if date_elements:
            comment_date = [
                datetime.strptime(date.get_attribute("datetime"), "%Y-%m-%dT%H:%M:%S.000Z").strftime("%Y-%m-%d %H:%M:%S")
                for date in date_elements
            ]
        else:
            date_now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            comment_date = self.handle_no_such_element(default_value=[date_now])
        return comment_date
    
    def comment_like(self):
        """
        Ekstrak jumlah like dari komentar.
        
        Returns:
            list: Daftar jumlah like komentar.
        """
        xpath_like = XpathComment.comment_likes
        like_elements = self.find_elements_with_fallback(By.XPATH, xpath_like)
        comment_like = [
            like.text.replace("likes", "").replace("like", "").strip() if "like" in like.text else "0"
            for like in like_elements
        ] if like_elements else self.handle_no_such_element(default_value=["0"])
        return comment_like
    
    def comment_link(self):
        """
        Ekstrak link dari komentar.
        
        Returns:
            list: Daftar link komentar.
        """
        xpath_link = XpathComment.comment_links
        link_elements = self.find_elements_with_fallback(By.XPATH, xpath_link)
        comment_link = [link.get_attribute("href") for link in link_elements] if link_elements else self.handle_no_such_element(default_value=[])
        return comment_link

